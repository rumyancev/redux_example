import {
    LOAD_ITEMS,
    ADD_ITEM,
    EDIT_ITEM,
    REMOVE_ITEM,
} from 'constants';

export function listItems(listItemsState = [], action) {
    switch(action.type){
        case LOAD_ITEMS:
            return action.payload;

        case ADD_ITEM:
            return [...listItemsState, {...action.payload}];

        case EDIT_ITEM:
            return listItemsState.map(function(item) {
                if(item.id === action.payload.id){
                    return {id: action.payload.id, text: action.payload.text};
                }

                return item;
            });

        case REMOVE_ITEM:
            return listItemsState.filter(item => item.id !== action.payload.id);

        default:
            return listItemsState;
    }
}